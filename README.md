# Synology-api-interact

## Description
A script that can connect to NAS synology API. Right now, it's used for automatically get the total size used on a NAS.
You can see the NAS Synology API Doc to see what's possible to do : [Documentation NAS Synology API](https://global.download.synology.com/download/Document/Software/DeveloperGuide/Package/FileStation/All/enu/Synology_File_Station_API_Guide.pdf)

## Usage
Use 

```bash
chmod u+x ./sizeOfNas.sh
./sizeOfNas.sh domainName login
```

You have to pass the domain name as $1 and the login id as $2.
You can write the password in the file at the line "password=" at the beginning.

## Contributing
You can contribute to the project, ameliorate the actual scripts and add new features.

Actually, the project is just at the begining. It works but not with all Synology NAS. Indeed, sometimes I get an html response instead of a json one. (Yet, I have a json response when I do the exact same request with my web browser).

## License
GNU GENERAL PUBLIC LICENSE Version 3
