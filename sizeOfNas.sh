#!/bin/bash

password=
#password=
declare -i giga=1073741824

#echo $password

# $1 = the name just before ".quickconnect.to" 
# $2 = admin login


# Ask for admin password
#echo -n password:
#read -s password
#echo


# API : Authentication Request
curl -s -o logintemp.json "https://$1.direct.quickconnect.to:63579/webapi/entry.cgi?api=SYNO.API.Auth&version=6&method=login&account=$2&passwd=$password&enable_syno_token=yes"
sleep 1

sid=$(cat logintemp.json | jq '.data.sid' | tr -d '"')
syno_token=$(cat logintemp.json | jq '.data.synotoken' | tr -d '"')

rm logintemp.json

# API : List all shared folders and write the json output in "sharedFolders.json" file
curl -s -o sharedFolders.json "https://$1.direct.quickconnect.to:63579/webapi/entry.cgi?api=SYNO.FileStation.List&version=2&method=list_share&additional=volume_status&_sid=$sid&SynoToken=$syno_token"

# Read the number of sharedFolders
total=$(cat sharedFolders.json | jq '.data.total')

# Declare an array and extract paths of sharedFolders. Store them in the array (replace '/' by '%2F' for the next API CALL; There must be only ONE '/' originaly)
# and another array to store taskIds
declare -a paths
declare -a taskId
for ((i=0; i<$total; i++))
do
	paths[$i]=$(cat sharedFolders.json | jq -r ".data.shares[$i].path" | tr -d '/' | tr ' ' '+')
	paths[$i]="%2F${paths[$i]}"
	curl -s -o taskId.json "https://$1.direct.quickconnect.to:63579/webapi/entry.cgi?api=SYNO.FileStation.DirSize&version=2&method=start&path=${paths[$i]}&_sid=$sid&SynoToken=$syno_token"
	sleep 1
	taskId[$i]=$(cat taskId.json | jq ".data.taskid")
done
sleep 30

# Once we have all the taskIds, we can check continuously if the calculations are done. If they are, then we store the size in an array
declare -a sizes
finalSize=0

for ((i=0; i<$total; i++))
do
	curl -s -o size.json "https://$1.direct.quickconnect.to:63579/webapi/entry.cgi?api=SYNO.FileStation.DirSize&version=2&method=status&taskid=${taskId[$i]}&_sid=$sid&SynoToken=$syno_token"
	finished=$(cat size.json | jq -r ".data.finished")
	#echo $finished

	if [[ "$finished" == "true" ]];
	then
		sizes[$i]=$(cat size.json | jq ".data.total_size")
		echo "Task $i - PATH: ${paths[$i]} - Size : ${sizes[$i]}"

		# We add all the sizes and we convert the finalSize in Gigabytes 
		LC_NUMERIC=C finalSize=$(printf "%.2f" "$(echo "$finalSize + ${sizes[$i]}/$giga" | bc)")
		#echo $finalSize

	# The else section steps backward to wait 30 seconds more and try again.
	else
		i=$((i-1))
		sleep 30
	fi
done

# clean temp files
rm size.json taskId.json sharedFolders.json 

echo
echo "Total = ${finalSize} Go"



